# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lportay <lportay@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/09/13 10:52:14 by lportay           #+#    #+#              #
#    Updated: 2019/01/15 16:54:18 by lportay          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: clean fclean re tags

SRCDIR= src

vpath %.c $(SRCDIR)
vpath %.h includes/

CC= gcc-8
CFLAGS= -Wall -Wextra -Werror $(INCLUDE)
DEBUG=prod
OPT=LIB
ARCH:= $(shell uname)

ifeq ($(ARCH), Darwin)
	CC= clang
else ifeq ($(ARCH), Linux)
	CC=gcc-8
endif

ifeq ($(DEBUG), yes)
	CFLAGS+= -g3
else ifeq ($(DEBUG), prod)
	CFLAGS+= -O3
else ifeq ($(DEBUG), sanitize)
	CFLAGS+= -g3 -fsanitize=address
endif

INCLUDE=\
-Iincludes\
-I$(LIBDIR)array\
-I$(LIBDIR)buf\
-I$(LIBDIR)bridge\
-I$(LIBDIR)hash\
-I$(LIBDIR)io\
-I$(LIBDIR)is\
-I$(LIBDIR)list\
-I$(LIBDIR)mem\
-I$(LIBDIR)obj\
-I$(LIBDIR)other\
-I$(LIBDIR)result\
-I$(LIBDIR)stack\
-I$(LIBDIR)str\
-I$(LIBDIR)string\
-I$(LIBDIR)utf\
-I$(LIBDIR)vector\
-I$(LIBDIR)wcs\

HEADERS= filler.h\

SRC= \
	 algo.c\
	 enemy.c\
	 filler.c\
	 init.c\
	 io.c\
	 main.c\
	 map.c\
	 pos.c\
	 valid.c\


OBJDIR= obj
OBJ= $(addprefix $(OBJDIR)/, $(SRC:%.c=%.o))

LIBDIR= libft/
LIB= libft.a

NAME= lportay.filler

GREEN="\033[32m"
RESET="\033[0m"

all: $(LIB) $(NAME)

$(NAME): $(LIBDIR)$(LIB) $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) -L$(LIBDIR) -lft
	@echo $(GREEN)$(NAME)" Successfully created"$(RESET)
	@mv $(NAME) resources/players

#the line above this comment shall be removed to avoid automatic relinkink

$(OBJDIR)/%.o: %.c $(HEADERS) | $(OBJDIR)
	$(COMPILE.c) $< -o $@ $(INCLUDE)

$(OBJDIR):
	-mkdir -p $@

$(LIB):
	@$(MAKE) -C $(LIBDIR)

$(LIBDIR)$(LIB):
	@$(MAKE) -C $(LIBDIR)

main: $(LIB)
	$(CC) $(CFLAGS) -o test $(main) -L$(LIBDIR) -lft
	-rm -f $(main:.c=.o)

tags:
	ctags -R *

clean:

ifeq ($(OPT), LIB)
	@$(MAKE) clean -C $(LIBDIR)
endif
	$(RM) -r $(OBJDIR) 
	@$(RM) test
	@$(RM) a.out
	@$(RM) -r test.dSYM

fclean: clean

ifeq ($(OPT), LIB)
	@$(MAKE) fclean -C $(LIBDIR)
endif		
	$(RM) $(NAME)
	@$(RM) -r $(NAME).dSYM

re : fclean all
