abanlin=resources/players/abanlin.filler
carli=resources/players/carli.filler
champely=resources/players/champely.filler
grati=resources/players/grati.filler
hcao=resources/players/hcao.filler
lp=resources/players/lportay.filler
sj=resources/players/superjeannot.filler
kaka=resources/players/kcosta.filler

m0=resources/maps/map_010
m1=resources/maps/map_011
m2=resources/maps/map_012
m3=resources/maps/map_013
m4=resources/maps/map_014
m5=resources/maps/map_015

cc0=resources/maps/cc0
cc1=resources/maps/cc1
cc2=resources/maps/cc2
cc3=resources/maps/cc3
cc4=resources/maps/cc4

bm=resources/maps/big_map
sm=resources/maps/small_map
mf=resources/maps/middle_fight

err_m=resources/maps/ERR_MAP
empty_m=resources/maps/EMPTY

vm=resources/filler_vm

RESET="\e[0m"
RED="\e[31m"
GREEN="\e[32m"
BLUE="\e[34m"


game() {
	echo -e "$GREEN`basename $2`$RESET (O) VS $RED`basename $3`$RESET (X) on $BLUE `basename $1`$RESET"
	r=$(./$vm -f $1 -p1 $2 -p2 $3 | tail -2)
	p1=$(echo $r | cut -d'=' -f3)
	p2=$(echo $r | cut -d'=' -f5)
	echo -e "$GREEN $p1 $RESET"
	echo -e "$RED $p2 $RESET"
}

beat_kcosta() {
	game $m0 $kaka $lp
	game $m0 $lp $kaka
	game $mf $kaka $lp
	game $mf $lp $kaka
	game $bm $kaka $lp
	game $bm $lp $kaka
	game $sm $kaka $lp
	game $sm $lp $kaka
}

cc0() {
	game $cc0 $lp $abanlin
	game $cc0 $lp $carli
	game $cc0 $lp $champely
	game $cc0 $lp $grati
	game $cc0 $lp $hcao
	game $cc0 $lp $sj
	game $cc0 $abanlin $lp
	game $cc0 $carli $lp
	game $cc0 $champely $lp
	game $cc0 $grati $lp
	game $cc0 $hcao $lp
	game $cc0 $sj $lp
}

cc1() {
	game $cc1 $lp $abanlin
	game $cc1 $lp $carli
	game $cc1 $lp $champely
	game $cc1 $lp $grati
	game $cc1 $lp $hcao
	game $cc1 $lp $sj
	game $cc1 $abanlin $lp
	game $cc1 $carli $lp
	game $cc1 $champely $lp
	game $cc1 $grati $lp
	game $cc1 $hcao $lp
	game $cc1 $sj $lp
}

cc2() {
	game $cc2 $lp $abanlin
	game $cc2 $lp $carli
	game $cc2 $lp $champely
	game $cc2 $lp $grati
	game $cc2 $lp $hcao
	game $cc2 $lp $sj
	game $cc2 $abanlin $lp
	game $cc2 $carli $lp
	game $cc2 $champely $lp
	game $cc2 $grati $lp
	game $cc2 $hcao $lp
	game $cc2 $sj $lp
}

cc3() {
	game $cc3 $lp $abanlin
	game $cc3 $lp $carli
	game $cc3 $lp $champely
	game $cc3 $lp $grati
	game $cc3 $lp $hcao
	game $cc3 $lp $sj
	game $cc3 $abanlin $lp
	game $cc3 $carli $lp
	game $cc3 $champely $lp
	game $cc3 $grati $lp
	game $cc3 $hcao $lp
	game $cc3 $sj $lp
}

cc4() {
	game $cc4 $lp $abanlin
	game $cc4 $lp $carli
	game $cc4 $lp $champely
	game $cc4 $lp $grati
	game $cc4 $lp $hcao
	game $cc4 $lp $sj
	game $cc4 $abanlin $lp
	game $cc4 $carli $lp
	game $cc4 $champely $lp
	game $cc4 $grati $lp
	game $cc4 $hcao $lp
	game $cc4 $sj $lp
}

tiny_map_test() {
	game $sm $abanlin $lp
	game $sm $carli $lp
	game $sm $champely $lp
	game $sm $grati $lp
	game $sm $hcao $lp
	game $sm $sj $lp
}

carli_beats_me() {
	game $m0 $carli $lp
	game $m0 $lp $carli
}

small_map_test() {
	game $m0 $abanlin $lp
	game $m0 $champely $lp
	game $m0 $grati $lp
	game $m0 $hcao $lp
	game $m0 $sj $lp
}
middle_fight_test() {
	game $mf $abanlin $lp
	game $mf $carli $lp
	game $mf $champely $lp
	game $mf $grati $lp
	game $mf $hcao $lp
	game $mf $sj $lp
}

big_map_test() {
	game $bm $abanlin $lp
	game $bm $carli $lp
	game $bm $champely $lp
	game $bm $grati $lp
	game $bm $hcao $lp
	game $bm $sj $lp
}

#MAIN
#middle_fight_test
#small_map_test
carli_beats_me
#cc0
#cc1
#cc2
#cc3
#cc4
