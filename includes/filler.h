/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 14:41:49 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 20:19:26 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include <limits.h>
# include "list.h"
# include "array.h"
# include "io.h"
# include "bridge.h"
# include "buf.h"

# undef BUFF_SIZE
# define BUFF_SIZE 16

# define RANGE "*0123456789ABCDDEFGHIJKLMNPQRSTUVWYZabcdefghijklmnpqrstuvwyz"
# define EMPTY 127
# define OCCUPIED 42

typedef struct s_filler	t_f;

typedef struct s_move	t_move;

/*
** 'O' or 'X' -> old piece
** 'o' or 'x' -> new piece
*/

/*
** Id can be either 'X' or 'O'
** p1 => 'O'
** p2 => 'X'
*/

struct					s_filler
{
	char				**map;
	char				**piece;
	unsigned			height;
	unsigned			width;
	unsigned			piece_height;
	unsigned			piece_width;
	unsigned			enemy_score;
	int					min_x;
	int					min_y;
	int					max_x;
	int					max_y;
	int					x;
	int					y;
	char				id;
	char				enemy;
	char				dead;
};

int						filler(void);
int						initialize(t_f *f);
int						dead_player(t_f *f);
unsigned				score(t_f *f, unsigned char **color_map);
int						valid(t_f *f);
int						map_size(unsigned *height, unsigned *width);
int						fill_map(char **map, unsigned width);
int						piece_size(unsigned *height, unsigned *width);
int						fill_piece(char **piece, unsigned width);
void					win_the_game(t_f *f);
void					finish_him(t_f *f);
int						min_x(char **a);
int						min_y(char **a);
int						real_piece_height(char **a);
int						real_piece_width(char **a);
void					colorize(char **map);
void					purify(char **map, char id);
void					combine(unsigned char **me, unsigned char **enemy);

#endif
