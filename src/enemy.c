/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   enemy.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 21:15:32 by lportay           #+#    #+#             */
/*   Updated: 2019/01/14 21:15:33 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

unsigned	player_score(char **map, char id)
{
	char		*row;
	unsigned	score;

	score = 0;
	while (*map)
	{
		row = *map;
		while (*row)
		{
			if (*row == id)
				score++;
			row++;
		}
		map++;
	}
	return (score);
}

int			dead_player(t_f *f)
{
	unsigned new_score;

	if ((new_score = player_score(f->map, f->enemy)) == f->enemy_score)
	{
		f->dead = 1;
		return (1);
	}
	else
	{
		f->enemy_score = new_score;
		return (0);
	}
}

void		finish_him(t_f *f)
{
	f->x = f->max_x;
	while (f->x >= f->min_x)
	{
		f->y = f->max_y;
		while (f->y >= f->min_y)
		{
			if (valid(f) == 1)
				return ;
			f->y--;
		}
		f->x--;
	}
}
