/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 15:06:48 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 20:18:05 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

/*
** Returns the player letter
*/

static int	player_id(void)
{
	char	*line;
	int		n;

	if (gnl(STDIN_FILENO, &line) == -1 || !line || ft_strlen(line) != 48)
		return (-1);
	n = ft_atol(line + 10);
	free(line);
	if (n == 1)
		return ('O');
	else if (n == 2)
		return ('X');
	else
		return (-1);
}

static int	enemy_id(char id)
{
	return ((id == 'O') ? 'X' : 'O');
}

int			initialize(t_f *f)
{
	f->dead = 0;
	f->enemy_score = 0;
	f->map = NULL;
	f->piece = NULL;
	if ((f->id = player_id()) == -1)
		return (-1);
	f->enemy = enemy_id(f->id);
	return (0);
}
