/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 21:15:57 by lportay           #+#    #+#             */
/*   Updated: 2019/01/14 21:15:58 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void	spread(char **map, int i, int j, char color)
{
	int height;
	int width;

	height = array_len((void **)map);
	width = ft_strlen(map[0]);
	if (i > 0 && map[i - 1][j] == EMPTY)
		map[i - 1][j] = color;
	if (i < height - 1 && map[i + 1][j] == EMPTY)
		map[i + 1][j] = color;
	if (j > 0 && map[i][j - 1] == EMPTY)
		map[i][j - 1] = color;
	if (j < width - 1 && map[i][j + 1] == EMPTY)
		map[i][j + 1] = color;
	if (i > 0 && j > 0 && map[i - 1][j - 1] == EMPTY)
		map[i - 1][j - 1] = color;
	if (i > 0 && j < width - 1 && map[i - 1][j + 1] == EMPTY)
		map[i - 1][j + 1] = color;
	if (j > 0 && i < height - 1 && map[i + 1][j - 1] == EMPTY)
		map[i + 1][j - 1] = color;
	if (j < width - 1 && i < height - 1 && map[i + 1][j + 1] == EMPTY)
		map[i + 1][j + 1] = color;
}

void		colorize(char **map)
{
	int h;
	int i;
	int j;

	h = 0;
	while (RANGE[h] != 'z')
	{
		i = 0;
		while (map[i])
		{
			j = 0;
			while (map[i][j])
			{
				if (map[i][j] == RANGE[h])
					spread(map, i, j, RANGE[h + 1]);
				j++;
			}
			i++;
		}
		h++;
	}
}

void		purify(char **map, char id)
{
	char *line;

	while (*map)
	{
		line = *map;
		while (*line)
		{
			if (*line == id)
				*line = OCCUPIED;
			else
				*line = EMPTY;
			line++;
		}
		map++;
	}
}

void		combine(unsigned char **me, unsigned char **enemy)
{
	int i;
	int j;

	i = 0;
	while (me[i])
	{
		j = 0;
		while (me[i][j])
		{
			if (me[i][j] == OCCUPIED || enemy[i][j] == OCCUPIED)
				me[i][j] = OCCUPIED;
			else
				me[i][j] += enemy[i][j];
			j++;
		}
		i++;
	}
}
