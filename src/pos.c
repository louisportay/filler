/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pos.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 15:06:48 by lportay           #+#    #+#             */
/*   Updated: 2018/02/13 16:02:56 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int	min_x(char **a)
{
	int i;

	i = 0;
	while (a[i])
	{
		if (ft_strchr(a[i], '*'))
			return (-i);
		i++;
	}
	return (0);
}

int	min_y(char **a)
{
	int i;
	int len;
	int ret;

	len = ft_strlen(*a);
	i = len;
	while (*a)
	{
		if ((ret = strchri(*a, '*')) >= 0 && ret < i)
			i = ret;
		a++;
	}
	if (i == len)
		return (0);
	return (-i);
}

int	real_piece_height(char **a)
{
	int i;

	i = array_len((void **)a);
	while (--i >= 0)
	{
		if (ft_strchr(a[i], '*'))
			return (i + 1);
	}
	return (0);
}

int	real_piece_width(char **a)
{
	int i;
	int ret;

	i = 0;
	while (*a)
	{
		if ((ret = strrchri(*a, '*')) > i)
			i = ret;
		a++;
	}
	return (i + 1);
}
