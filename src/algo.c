/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 21:15:24 by lportay           #+#    #+#             */
/*   Updated: 2019/01/14 21:15:25 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

/*
** The lower the score, the better
*/

void	best_spot(t_f *f, unsigned char **color_map, int x, int y)
{
	unsigned	best_score;
	unsigned	ret;

	best_score = UINT_MAX;
	f->x = f->min_x;
	while (f->x <= f->max_x)
	{
		f->y = f->min_y;
		while (f->y <= f->max_y)
		{
			if (valid(f) == 1 && (ret = score(f, color_map)) < best_score)
			{
				best_score = ret;
				x = f->x;
				y = f->y;
			}
			f->y++;
		}
		f->x++;
	}
	f->x = x;
	f->y = y;
}

void	win_the_game(t_f *f)
{
	unsigned char **me;
	unsigned char **enemy;

	me = (unsigned char **)array_dup((void **)f->map,
										array_len((void **)f->map),
										(void *(*)(void *))&ft_strdup, &free);
	enemy = (unsigned char **)array_dup((void **)f->map,
										array_len((void **)f->map),
										(void *(*)(void *))&ft_strdup, &free);
	if (!me || !enemy)
		return (finish_him(f));
	purify((char **)me, f->id);
	purify((char **)enemy, f->enemy);
	colorize((char **)me);
	colorize((char **)enemy);
	combine(me, enemy);
	array_destroy((void ***)&enemy, array_len((void **)enemy), &free);
	best_spot(f, me, -1, -1);
	array_destroy((void ***)&me, array_len((void **)me), &free);
}
