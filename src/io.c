/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   io.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 21:15:48 by lportay           #+#    #+#             */
/*   Updated: 2019/01/14 21:15:49 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int	fail(char *line)
{
	free(line);
	return (-1);
}

/*
** Mutate weight and width with the right dimensions
*/

int			map_size(unsigned *height, unsigned *width)
{
	char	*line;
	char	**a;

	if (gnl(STDIN_FILENO, &line) == -1)
		return (-1);
	if (!(a = strsplit(line, ' ')))
		return (fail(line));
	free(line);
	if (array_len((void **)a) != 3)
	{
		array_destroy((void ***)&a, array_len((void **)a), &free);
		return (-1);
	}
	*height = ft_atol(a[1]);
	*width = ft_atol(a[2]);
	array_destroy((void ***)&a, array_len((void **)a), &free);
	if (*height == 0 || *width == 0)
		return (-1);
	return (0);
}

int			fill_map(char **map, unsigned width)
{
	char		*line;
	char		**iter;
	unsigned	len;

	if (gnl(STDIN_FILENO, &line) == -1)
		return (-1);
	if (ft_strncmp(line, "    0", 5))
		return (fail(line));
	free(line);
	iter = map;
	while (*iter)
	{
		if (gnl(STDIN_FILENO, &line) == -1)
			return (-1);
		if ((len = ft_strlen(line)) != width + 4)
			return (fail(line));
		ft_strncpy(*iter++, (line + 4), width + 1);
		free(line);
	}
	return (0);
}

int			piece_size(unsigned *height, unsigned *width)
{
	char	**a;
	char	*line;

	if (gnl(STDIN_FILENO, &line) == -1)
		return (-1);
	if (!(a = strsplit(line, ' ')))
		return (fail(line));
	free(line);
	if (array_len((void **)a) != 3)
	{
		array_destroy((void ***)&a, array_len((void **)a), &free);
		return (-1);
	}
	*height = ft_atol(a[1]);
	*width = ft_atol(a[2]);
	array_destroy((void ***)&a, array_len((void **)a), &free);
	if (*height == 0 || *width == 0)
		return (-1);
	return (0);
}

int			fill_piece(char **piece, unsigned width)
{
	char		*line;
	unsigned	len;

	while (*piece)
	{
		if (gnl(STDIN_FILENO, &line) == -1)
			return (-1);
		if ((len = ft_strlen(line)) != width)
			return (fail(line));
		ft_strncpy(*piece, line, width + 1);
		piece++;
		free(line);
	}
	return (0);
}
