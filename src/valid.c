/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/14 21:15:39 by lportay           #+#    #+#             */
/*   Updated: 2019/01/14 21:15:39 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int	is_id(char c, char id)
{
	return (c == id || ft_toupper(c) == id);
}

static int	is_enemy(char c, char enemy)
{
	return (c == enemy || ft_toupper(c) == enemy);
}

unsigned	score(t_f *f, unsigned char **color_map)
{
	unsigned	score;
	int			i;
	int			j;

	score = 0;
	i = 0;
	while (f->piece[i])
	{
		j = 0;
		while (f->piece[i][j])
		{
			if (f->piece[i][j] == '*')
			{
				if (color_map[f->x + i][f->y + j] != '*')
					score += color_map[f->x + i][f->y + j];
			}
			j++;
		}
		i++;
	}
	return (score);
}

/*
** Returns 1 if valid, 0 or -1 otherwise
** Not secure, watch out for buffer overflow
*/

int			valid(t_f *f)
{
	int	bond;
	int	i;
	int	j;

	bond = 0;
	i = 0;
	while (f->piece[i])
	{
		j = 0;
		while (f->piece[i][j])
		{
			if (f->piece[i][j] == '*')
			{
				if (is_id(f->map[f->x + i][f->y + j], f->id))
					bond = (bond == 0) ? 1 : -1;
				else if (is_enemy(f->map[f->x + i][f->y + j], f->enemy))
					return (0);
			}
			j++;
		}
		i++;
	}
	return (bond);
}
