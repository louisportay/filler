/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 15:06:48 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 20:08:42 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void	print(int x, int y)
{
	t_buf		b;

	buf_init(&b, STDOUT_FILENO);
	buf_l(&b, x);
	buf_c(&b, ' ');
	buf_l(&b, y);
	buf_c(&b, '\n');
	buf_reset(&b);
}

static void	place_piece(t_f *f)
{
	f->min_x = min_x(f->piece);
	f->min_y = min_y(f->piece);
	f->max_x = f->height - real_piece_height(f->piece);
	f->max_y = f->width - real_piece_width(f->piece);
	if (f->dead == 1 || dead_player(f) == 1)
		finish_him(f);
	else
		win_the_game(f);
	print(f->x, f->y);
}

static int	handle_io(t_f *f)
{
	if (map_size(&f->height, &f->width) == -1)
		return (-1);
	if (!(f->map = (char **)array_w_new(f->height,
										sizeof(char) * (f->width + 1))))
		return (-1);
	if (fill_map(f->map, f->width) == -1)
		return (-1);
	if (piece_size(&f->piece_height, &f->piece_width) == -1)
		return (-1);
	if (!(f->piece = (char **)array_w_new(f->piece_height, sizeof(char) *
											(f->piece_width + 1))))
		return (-1);
	if (fill_piece(f->piece, f->piece_width) == -1)
		return (-1);
	return (0);
}

int			filler(void)
{
	t_f	f;

	initialize(&f);
	while (1)
	{
		if (handle_io(&f) == -1)
		{
			array_destroy((void ***)&f.map, array_len((void **)f.map), &free);
			array_destroy((void ***)&f.piece, array_len((void **)f.piece),
														&free);
			write(STDOUT_FILENO, "-1 -1\n", 6);
			return (-1);
		}
		place_piece(&f);
		array_destroy((void ***)&f.map, array_len((void **)f.map), &free);
		array_destroy((void ***)&f.piece, array_len((void **)f.piece), &free);
	}
	return (0);
}
