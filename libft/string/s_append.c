/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_append.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 12:50:07 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:07:42 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gstring.h"

int	s_append(const char *str, t_string *s)
{
	wchar_t *w_str;
	size_t	w_len;

	if (!(w_str = utf_decode(str, ft_strlen(str))))
		return (-1);
	w_len = ft_wcslen(w_str);
	while (s->used + w_len > s->size)
	{
		if (s_grow(s) == -1)
		{
			free(w_str);
			return (-1);
		}
	}
	ft_wcscat(s->s, w_str);
	s->used += w_len;
	free(w_str);
	return (0);
}
