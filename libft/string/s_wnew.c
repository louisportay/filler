/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_wnew.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 14:00:00 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 15:02:50 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gstring.h"

t_string	*s_wnew(const wchar_t *w_str)
{
	t_string	*new;

	if (!(new = malloc(sizeof(t_string))))
		return (NULL);
	if (!(new->s = ft_wcsdup(w_str)))
	{
		free(new);
		return (NULL);
	}
	new->size = ft_wcslen(w_str);
	new->used = new->size;
	return (new);
}
