/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_extract.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 13:28:48 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 13:59:41 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gstring.h"

char	*s_extract(t_string *s)
{
	char *str;

	if (!(str = utf_encode(s->s, s->used)))
		return (NULL);
	free(s->s);
	free(s);
	return (str);
}
