/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_new.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 13:49:29 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 15:02:23 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gstring.h"

t_string	*s_new(const char *str)
{
	t_string	*new;

	if (!(new = malloc(sizeof(t_string))))
		return (NULL);
	new->size = ft_strlen(str);
	if (!(new->s = utf_decode(str, new->size)))
	{
		free(new);
		return (NULL);
	}
	new->used = ft_wcslen(new->s);
	return (new);
}
