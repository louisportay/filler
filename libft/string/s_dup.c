/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_dup.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 13:22:12 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 16:27:39 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "gstring.h"

t_string	*s_dup(t_string *s)
{
	t_string *new;

	if (!(new = malloc(sizeof(t_string))))
		return (NULL);
	if (!(new->s = malloc(sizeof(wchar_t) * s->size)))
	{
		free(new);
		return (NULL);
	}
	ft_wcscpy(new->s, s->s);
	new->used = s->used;
	new->size = s->size;
	return (new);
}
