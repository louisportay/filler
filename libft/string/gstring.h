/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gstring.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 12:16:16 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:38:50 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GSTRING_H
# define GSTRING_H

# include <wchar.h>
# include <unistd.h>
# include "utf.h"

typedef struct	s_string
{
	wchar_t		*s;
	size_t		size;
	size_t		used;
}				t_string;

int				s_grow(t_string *s);
void			s_del(t_string **s);
void			s_print(t_string *s);
t_string		*s_dup(t_string *s);
size_t			s_len(const t_string *s);
int				s_cmp(const t_string *s1, const t_string *s2);
t_string		*s_new(const char *str);
int				s_append(const char *str, t_string *s);
char			*s_extract(t_string *s);
t_string		*s_wnew(const wchar_t *w_str);
int				s_wappend(const wchar_t *w_str, t_string *s);
wchar_t			*s_wextract(t_string *s);

#endif
