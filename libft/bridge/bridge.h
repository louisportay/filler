/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bridge.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 13:48:42 by lportay           #+#    #+#             */
/*   Updated: 2018/12/17 17:10:49 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BRIDGE_H
# define BRIDGE_H

# include <stdlib.h>
# include "is.h"
# include "list.h"

long		ft_atol(const char *nptr);
char		*ltoa(long n);
unsigned	lwidth(long nb);
long		ft_abs(long n);
void		lswap(long *a, long *b);
void		iswap(int *a, int *b);

#endif
