/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lwidth.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 17:06:16 by lportay           #+#    #+#             */
/*   Updated: 2018/12/17 17:07:29 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bridge.h"

unsigned	lwidth(long nb)
{
	int				width;
	unsigned long	nbr;

	nbr = (nb > 0) ? nb : -nb;
	width = (nb >= 0) ? 1 : 2;
	while ((nbr /= 10) != 0)
		width++;
	return (width);
}
