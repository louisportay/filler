/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 16:40:35 by lportay           #+#    #+#             */
/*   Updated: 2018/12/13 19:11:38 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTF_H
# define UTF_H

# include "wcs.h"
# include "str.h"
# include "string.h"

wchar_t	*utf_decode(const char *str, size_t len);
char	*utf_encode(const wchar_t *w_str, size_t len);

#endif
