/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_insert.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 16:32:03 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:21:28 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash.h"

void	hash_insert(t_hash **table, t_hash *bucket, int crush, t_hash_utils *u)
{
	t_hash		*h;
	unsigned	i;

	if (!(h = hash_lookup(table, bucket->key, u)))
	{
		i = hash_index(bucket->key);
		bucket->next = table[i];
		table[i] = bucket;
	}
	else if (crush == 1)
	{
		u->del_key(bucket->key);
		u->del_data(h->data);
		h->data = bucket->data;
		free(bucket);
	}
}

int		hash_insert_data(t_hash **table, t_kvp p, int crush, t_hash_utils *u)
{
	t_result	new;
	t_hash		*h;
	unsigned	i;

	if (!(h = hash_lookup(table, p.key, u)))
	{
		new = hash_new(p.key, p.data);
		if (new.status != OK)
			return (-1);
		i = hash_index(p.key);
		((t_hash *)new.data)->next = table[i];
		table[i] = ((t_hash *)new.data);
	}
	else if (crush == 1)
	{
		u->del_key(p.key);
		u->del_data(h->data);
		h->data = p.data;
	}
	return (0);
}

int		hash_insert_dup(t_hash **table, t_kvp p, int crush, t_hash_utils *u)
{
	t_result	new;
	t_hash		*h;
	unsigned	i;

	if (!(h = hash_lookup(table, p.key, u)))
	{
		if (!(p.key = u->dup_key(p.key)) ||
				!(p.data = u->dup_data(p.data)))
			return (-1);
		new = hash_new(p.key, p.data);
		if (new.status != OK)
			return (-1);
		i = hash_index(p.key);
		((t_hash *)new.data)->next = table[i];
		table[i] = ((t_hash *)new.data);
	}
	else if (crush == 1)
	{
		u->del_data(h->data);
		if (!(p.data = u->dup_data(p.data)))
			return (-1);
		h->data = p.data;
	}
	return (0);
}
