/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_lookup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 16:29:53 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 16:09:29 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash.h"

/*
** Search inside the whole table the bucket with the given 'key'
** Stacked buckets are inspected
** returns a pointer to the bucket if successful, NULL otherwise
*/

t_hash	*hash_lookup(t_hash **table, void *key, t_hash_utils *u)
{
	t_hash *h;

	h = table[hash_index(key)];
	while (h)
	{
		if (u->eq(key, h->key) == 1)
			return (h);
		h = h->next;
	}
	return (NULL);
}
