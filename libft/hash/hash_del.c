/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_del.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 16:36:42 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 16:10:42 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash.h"

/*
** Delete the bucket with the given 'key' in the 'table' with 'del'
** The stacked buckets are searched and the links are preserved.
*/

t_kvp	hash_extract(t_hash **table, void *key, t_hash_utils *u)
{
	t_hash	*iter;
	t_hash	*prev;
	t_kvp	p;

	p.key = NULL;
	p.data = NULL;
	iter = table[hash_index(key)];
	if (u->eq(key, iter->key) == 1)
		table[hash_index(key)] = iter->next;
	prev = iter;
	while (iter)
	{
		if (u->eq(key, iter->key) == 1)
		{
			prev->next = iter->next;
			p.key = iter->key;
			p.data = iter->data;
			free(iter);
			return (p);
		}
		prev = iter;
		iter = iter->next;
	}
	return (p);
}

void	hashdel(t_hash **table, char *key, t_hash_utils *u)
{
	t_hash *iter;
	t_hash *prev;

	iter = table[hash_index(key)];
	if (u->eq(key, iter->key) == 1)
		table[hash_index(key)] = iter->next;
	prev = iter;
	while (iter != NULL)
	{
		if (u->eq(key, iter->key) == 1)
		{
			prev->next = iter->next;
			u->del_key(iter->key);
			u->del_data(iter->data);
			free(iter);
		}
		prev = iter;
		iter = iter->next;
	}
}

void	hash_clear(t_hash **table, t_hash_utils *u)
{
	t_hash		*iter;
	t_hash		*next;
	unsigned	i;

	i = 0;
	while (i < HASHSIZE)
	{
		if (table[i])
			iter = table[i];
		while (iter)
		{
			next = iter->next;
			u->del_key(iter->key);
			u->del_data(iter->data);
			free(iter);
			iter = next;
		}
		i++;
	}
}
