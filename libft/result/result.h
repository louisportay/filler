/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   result.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 15:54:25 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 18:13:26 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RESULT_H
# define RESULT_H

enum	e_result
{
	OK,
	ERR,
	EMPTY,
	NOMEM
};

typedef struct		s_result
{
	void			*data;
	enum e_result	status;
}					t_result;

t_result			result_early(t_result *r, enum e_result st);

#endif
