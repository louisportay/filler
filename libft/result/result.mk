RESULT=\
	   result_utils.c\


RESULT_DIR=result

OBJ+=$(addprefix $(OBJDIR)/$(RESULT_DIR)/, $(RESULT:%.c=%.o))

SRCDIR+=$(RESULT_DIR)/

INCLUDE+=-I$(RESULT_DIR)/

HEADER=result

$(OBJDIR)/$(RESULT_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(RESULT_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(RESULT_DIR):
	mkdir -p $(OBJDIR)/$(RESULT_DIR)
