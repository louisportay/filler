STACK=\
stack_destroy.c\
stack_dup.c\
stack_new.c\
stack_pop.c\
stack_map.c\
stack_push.c\
stack_utils.c


STACK_DIR=stack

OBJ+=$(addprefix $(OBJDIR)/$(STACK_DIR)/, $(STACK:%.c=%.o))

SRCDIR+=$(STACK_DIR)/

INCLUDE+=-I$(STACK_DIR)/

HEADER=stack

$(OBJDIR)/$(STACK_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(STACK_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(STACK_DIR):
	mkdir -p $(OBJDIR)/$(STACK_DIR)
