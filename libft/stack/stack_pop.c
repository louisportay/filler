/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_pop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 18:07:04 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 10:53:24 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

void	*stack_pop(t_stk **stk)
{
	t_stk	*uppest_member;
	void	*ret;

	uppest_member = *stk;
	ret = uppest_member->data;
	*stk = uppest_member->down;
	free(uppest_member);
	return (ret);
}

void	stack_del(t_stk **stk, void (*del)(void *))
{
	t_stk *uppest_member;

	uppest_member = *stk;
	*stk = uppest_member->down;
	del(uppest_member->data);
	free(uppest_member);
}
