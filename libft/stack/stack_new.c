/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/26 17:59:45 by lportay           #+#    #+#             */
/*   Updated: 2018/12/09 19:37:09 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

t_stk	*stack_new(void *data)
{
	t_stk *new;

	if (!(new = malloc(sizeof(t_stk))))
		return (NULL);
	new->data = data;
	new->down = NULL;
	return (new);
}
