/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_dup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/08 17:28:26 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 18:15:14 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

t_result	stack_dup(t_stk *stk, void *(*dup)(void *), void (*del)(void *))
{
	t_result	r;
	t_stk		*iter;
	void		*cpy;

	r.data = NULL;
	if (!stk)
		return (result_early(&r, EMPTY));
	if (stack_push_dup((t_stk **)&r.data, stk->data, dup) == -1)
		return (result_early(&r, NOMEM));
	r.status = OK;
	stk = stk->down;
	iter = (t_stk *)r.data;
	while (stk)
	{
		if (!(cpy = dup(stk->data)) ||
				!(iter->down = stack_new(cpy)))
		{
			stack_destroy((t_stk **)&r.data, del);
			return (result_early(&r, NOMEM));
		}
		iter = iter->down;
		stk = stk->down;
	}
	return (r);
}
