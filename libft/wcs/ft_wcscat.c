/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcscat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/14 14:50:30 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:58:54 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wcs.h"

wchar_t	*ft_wcscat(wchar_t *dest, const wchar_t *src)
{
	wchar_t *iter;

	iter = dest;
	while (*iter)
		iter++;
	while (*src)
		*iter++ = *src++;
	*iter = L'\0';
	return (dest);
}

wchar_t	*ft_wcsncat(wchar_t *dest, const wchar_t *src, size_t n)
{
	size_t	i;
	size_t	j;

	i = ft_wcslen(dest);
	j = 0;
	while (src[j] && j < n)
		dest[i++] = src[j++];
	dest[i] = '\0';
	return (dest);
}
