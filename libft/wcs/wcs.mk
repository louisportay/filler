WCS=\
	ft_wcscpy.c\
	ft_wcsdup.c\
	ft_wcslen.c\
	ft_wcsncpy.c\
	ft_wcscmp.c\
	ft_wcscat.c\
	wcsnew.c\
	wcsbytes.c\

WCS_DIR=wcs

OBJ+=$(addprefix $(OBJDIR)/$(WCS_DIR)/, $(WCS:%.c=%.o))

SRCDIR+=$(WCS_DIR)/

INCLUDE+=-I$(WCS_DIR)/

HEADER=wcs

$(OBJDIR)/$(WCS_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(WCS_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(WCS_DIR):
	mkdir -p $(OBJDIR)/$(WCS_DIR)
