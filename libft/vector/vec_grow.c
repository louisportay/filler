/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_grow.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 11:47:40 by lportay           #+#    #+#             */
/*   Updated: 2018/12/13 13:02:36 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

int			vec_grow(t_vector *vec)
{
	void		**bigger_map;
	unsigned	u;

	if (!(bigger_map = array_new(vec->size * 2)))
		return (-1);
	u = 0;
	while (u < vec->size)
	{
		bigger_map[u] = vec->map[u];
		u++;
	}
	free(vec->map);
	vec->map = bigger_map;
	vec->size *= 2;
	return (0);
}
