VECTOR=\
	vec_append.c\
	vec_del.c\
	vec_grow.c\
	vec_new.c\
	vec_utils.c\



VECTOR_DIR=vector

OBJ+=$(addprefix $(OBJDIR)/$(VECTOR_DIR)/, $(VECTOR:%.c=%.o))

SRCDIR+=$(VECTOR_DIR)/

INCLUDE+=-I$(VECTOR_DIR)/

HEADER=vector

$(OBJDIR)/$(VECTOR_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(VECTOR_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(VECTOR_DIR):
	mkdir -p $(OBJDIR)/$(VECTOR_DIR)
