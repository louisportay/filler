/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 10:43:33 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 16:11:10 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_H
# define VECTOR_H

# include <stdlib.h>
# include "array.h"

typedef struct	s_vector
{
	void		**map;
	size_t		used;
	size_t		size;
}				t_vector;

/*
** vec_shrink ?
**
** Be careful with 'vec_del', all the data inside it will be freed with one
** single function ('del'), so you can only have one structure type inside
*/

t_vector		vec_new(unsigned size);
int				vec_err(t_vector *vec);
int				vec_append(t_vector *vec, void *data);
int				vec_append_dup(t_vector *vec, void *data, void *(*dup)(void *));
int				vec_grow(t_vector *vec);
void			*vec_extract(t_vector *vec, unsigned i);
void			vec_del(t_vector *vec, unsigned i, void (*del)(void *));
void			vec_destroy(t_vector *vec, void (*del)(void *));

#endif
