/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_new.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 11:05:09 by lportay           #+#    #+#             */
/*   Updated: 2018/12/11 13:42:51 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

t_vector	vec_new(unsigned size)
{
	t_vector vec;

	if (!(vec.map = (void **)malloc(sizeof(void *) * size)))
		return (vec);
	vec.used = 0;
	vec.size = size;
	return (vec);
}
