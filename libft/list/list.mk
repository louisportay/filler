LIST=\
lst_add_back.c\
lst_add_front.c\
lst_del.c\
lst_dup.c\
lst_filter.c\
lst_join.c\
lst_map.c\
lst_new.c\
lst_rev.c\
lst_sort.c\
lst_utils.c\
lst_search.c\


LIST_DIR=list

OBJ+=$(addprefix $(OBJDIR)/$(LIST_DIR)/, $(LIST:%.c=%.o))

SRCDIR+=$(LIST_DIR)/

INCLUDE+=-I$(LIST_DIR)/

HEADER=list

$(OBJDIR)/$(LIST_DIR)/%.o: %.c $(HEADER) | $(OBJDIR)/$(LIST_DIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR)/$(LIST_DIR):
	mkdir -p $(OBJDIR)/$(LIST_DIR)
