/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 15:10:05 by lportay           #+#    #+#             */
/*   Updated: 2018/12/14 14:26:16 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H

# include <stdlib.h>
# include "result.h"

typedef struct s_lst	t_lst;

struct		s_lst
{
	void	*data;
	t_lst	*next;
};

/*
** /!\ pseudo functional *map* doesn't duplicate data !
** It mutates it instead (so maybe dup your list  before using it !)
**
** filter returns a newly allocated list
**
** Avoid as much as possible *Back* functions
** They've got O(n) complexity whereas *Front* functions got O(1)
**
** It is safe to always re-assign 'head' with the result of 'lst_join'
*/

t_lst		*lst_new(void *data);
void		lst_addback(t_lst **l, t_lst *elem);
int			lst_addrback_data(t_lst **l, void *data);
int			lst_addback_dup(t_lst **l, void *data, void *(*dup)(void *));
void		lst_addfront(t_lst **l, t_lst *elem);
int			lst_addfront_data(t_lst **l, void *data);
int			lst_addfront_dup(t_lst **l, void *data, void *(*dup)(void *));
void		*lst_dequeueback(t_lst **l);
void		*lst_dequeuefront(t_lst **l);
void		lst_delback(t_lst **l, void (*del)(void *));
void		lst_delfront(t_lst **l, void (*del)(void *));
void		lst_destroy(t_lst **l, void (*del)(void *));
void		lst_rev(t_lst **l);
void		lst_map(t_lst *l, void (*fn)(void *));
t_result	lst_filter(t_lst *l, int (*slct)(void *), void *(*dup)(void *),
						void (*del)(void *));
void		lst_sort(t_lst **l, int (*cmp)(void *, void *));
t_lst		*lst_select(t_lst *l, int (*cmp)(void *, void *));
t_lst		*lst_search(t_lst *l, int (*slct)(void *));
t_result	lst_dup(t_lst *l, void *(*dup)(void *), void (*del)(void *));
unsigned	lst_len(t_lst *l);
t_lst		*lst_tail(t_lst *l);
t_lst		*lst_join(t_lst *head, t_lst *tail);
void		*lst_data(t_lst *elem);

#endif
