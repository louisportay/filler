/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_sort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 12:08:45 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 15:43:19 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

/*
** parts[0] -> less
** parts[1] -> equal
** parts[2] -> greater
*/

static void	init(t_lst **parts, t_lst **iter, t_lst **pivot, t_lst *l)
{
	parts[0] = NULL;
	parts[1] = NULL;
	parts[2] = NULL;
	*iter = l;
	*pivot = l;
}

void		lst_sort(t_lst **l, int (*cmp)(void *, void *))
{
	t_lst	*parts[3];
	t_lst	*iter;
	t_lst	*pivot;
	int		ret;

	if (lst_len(*l) < 2)
		return ;
	init(parts, &iter, &pivot, *l);
	while (iter)
	{
		*l = (*l)->next;
		if ((ret = cmp(iter->data, pivot->data)) < 0)
			lst_addfront(&parts[0], iter);
		else if (ret == 0)
			lst_addfront(&parts[1], iter);
		else if (ret > 0)
			lst_addfront(&parts[2], iter);
		iter = *l;
	}
	lst_sort(&parts[0], cmp);
	lst_sort(&parts[2], cmp);
	parts[1] = lst_join(parts[1], parts[2]);
	parts[0] = lst_join(parts[0], parts[1]);
	*l = parts[0];
}
