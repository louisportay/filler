/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_add_front.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 15:47:50 by lportay           #+#    #+#             */
/*   Updated: 2018/12/06 17:25:04 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void	lst_addfront(t_lst **l, t_lst *elem)
{
	elem->next = *l;
	*l = elem;
}

int		lst_addfront_data(t_lst **l, void *data)
{
	t_lst *new;

	if (!(new = lst_new(data)))
		return (-1);
	new->next = *l;
	*l = new;
	return (0);
}

int		lst_addfront_dup(t_lst **l, void *data, void *(*dup)(void *))
{
	void	*cpy;
	t_lst	*new;

	if (!(cpy = dup(data)))
		return (-1);
	if (!(new = lst_new(cpy)))
	{
		free(cpy);
		return (-1);
	}
	new->next = *l;
	*l = new;
	return (0);
}
