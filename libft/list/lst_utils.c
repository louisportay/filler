/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 15:25:29 by lportay           #+#    #+#             */
/*   Updated: 2018/12/11 12:14:11 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

unsigned	lst_len(t_lst *l)
{
	unsigned len;

	len = 0;
	while (l)
	{
		len++;
		l = l->next;
	}
	return (len);
}

t_lst		*lst_tail(t_lst *l)
{
	while (l && l->next)
		l = l->next;
	return (l);
}

/*
** The data returned by this function should NEVER be modified. Only read.
*/

void		*lst_data(t_lst *elem)
{
	return (elem->data);
}
