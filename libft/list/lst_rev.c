/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_rev.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 15:02:17 by lportay           #+#    #+#             */
/*   Updated: 2018/12/07 15:02:27 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void	lst_rev(t_lst **l)
{
	t_lst *prev;
	t_lst *current;
	t_lst *next;

	prev = NULL;
	current = *l;
	while (current)
	{
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	*l = prev;
}
