/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_join.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/10 12:47:47 by lportay           #+#    #+#             */
/*   Updated: 2018/12/10 13:20:39 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_lst	*lst_join(t_lst *head, t_lst *tail)
{
	if (!head)
		return (tail);
	if (!tail)
		return (head);
	lst_tail(head)->next = tail;
	return (head);
}
